<?php
//PHP5
//nhomvn
@apache_setenv('no-gzip', 1);
@ini_set('zlib.output_compression', 0);
@ini_set('implicit_flush', 1);
ob_implicit_flush(TRUE);
require("frontengine.php");
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<? if(!$custom_title) { ?>
<title>PURtel.com GmbH VoIP - PBX/ACD - WebCall - Web2Fax</title>
<? } else { echo $custom_title; } ?>
<? if(!$custom_keywords) { ?>
<meta name="Keywords" content="VoIP, IP, hosted PBX, ACD,Centrex, Whitelabel, Lizenz, Lizenzen,
Telefonanlage, hosted, SIP, IAX, IAX2, telefon, telefonieren, flatrate,
Reseller, Lsung, Lsungen, Voice over IP, Hersteller, Solution, Callcenter, 
call center, callshop, callingcard, calling card, Provider, 
Internettelefonie, telefonieren, pbx, telefonie, terminieren, wholesale">
<? } else { echo $custom_keywords; } ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">  
<? if($marke=="Nebenstellen" && $vollbild) { ?>
<meta http-equiv="Page-Enter" content="blendTrans(Duration=1.0)">
<meta http-equiv="Site-Enter" content="blendTrans(Duration=1.0)">
<meta http-equiv="Page-Exit" content="blendTrans(Duration=1.0)">
<meta http-equiv="Site-Exit" content="blendTrans(Duration=1.0)">
<? } ?>

<script type="text/javascript">
function WebOeffnen (Adresse, Ziel) {
  WebFenster = window.open(Adresse, Ziel, "width=480, height=272, clientWidth=480, clientHeight=272, innerWidth=480, innerHeight=272, scrollbars=no, location=no, locationbar=no, menubar=no, personalbar=no, statusbar=no, status=no, toolbar=no, hotkeys=no, resizable=no");
  WebFenster.moveTo(screen.availWidth/2-240,screen.availHeight/2-161);
  WebFenster.focus();
}
function IpTVOeffnen (Adresse, Ziel) {
  IpTVFenster = window.open(Adresse, Ziel, "fullscreen=yes, scrollbars=no");
  IpTVFenster.focus();
}
</script>
<script src="javascripts/jquery-2.2.3.min.js"></script>
<script src="javascripts/jquery.dynatable.js"></script>
<script src="javascripts/jquery-ui.min.js"></script>
<? if($site=="fax") { ?>
<script language="javascript" src="cal2.js"></script> <!-- Javascript fr Kalender-->
<script language="javascript" src="cal_conf2.js"></script> <!-- Javascript fr Kalender-->
<? } ?>

<script type="text/javascript" language="javascript">

  <? if($fullscreen=="1") { ?>
  window.moveTo(0,0);
  window.resizeTo(screen.availWidth,screen.availHeight);
  <? } ?>

  function Verlinken (formular){
    var selektiert = formular.elements['art_gruppe'].selectedIndex;
    formular.action = 'index.php';
    formular.submit();
  }

  function Verlinken2 (formular){
    var selektiert = formular.elements['language'].selectedIndex;
    formular.action = 'index.php';
    formular.submit();
  }
  function openMyWin(url, win, width, height, x) {
      var e = ',menubar=no,status=no,toolbar=no,location=no,resizable=yes';
      var param = 'scrollbars=yes,width=' + width + ',height=' + height + (x ? e : '');
      window.open(url, win, param).focus();
  }
</script>

<? if(!$custom_robots) { ?><? } else { echo $custom_robots; } ?>
<link rel="stylesheet" href="style.css" type="text/css">
<link rel="stylesheet" href="jquery-ui.min.css" type="text/css">
<link rel="stylesheet" href="jquery-ui.theme.min.css" type="text/css">
<?
  echo "<link rel=\"stylesheet\" href=\"$custom_stylesheet\" type=\"text/css\">";
?>
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" <? echo $load_text; ?> class="main">
<? if(!$vollbild) { ?>
<? if(!$login_only) { ?>
<table width="992" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="992" height="40">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="4">&nbsp;</td>
          <td width="992" valign="middle">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td align="left">
                  <? /* ?>
                  <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td align="left" valign="bottom" style="font-family: Verdana, Arial, Helvetica, sans-serif; padding:0px 0px 0px 0px; font-size: 30px; color: #F29210; font-weight: bold;">PURtel</td>
                      <td align="left" valign="bottom" style="font-family: Verdana, Arial, Helvetica, sans-serif; padding:0px 0px 2px 0px; font-size: 20px; color: #888888;">.com</td>
                      <td align="left" valign="bottom" style="font-family: Verdana, Arial, Helvetica, sans-serif; padding:0px 0px 20px 0px; font-size: 20px; color: #888888;"><img src="images/sprechblase2.gif" border="0"></td>
                      <td align="left" valign="bottom" style="font-family: Verdana, Arial, Helvetica, sans-serif; padding:0px 0px 40px 0px; font-size: 8px; color: #000000;">&reg;</td>
                    </tr>
                  </table>
                  <? */ ?>
                  <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <? /* ?>
                    <tr>
                      <td align="left" valign="bottom" style="font-family: Verdana, Arial, Helvetica, sans-serif; padding:0px 0px 0px 0px; font-size: 30px; color: #F29210; font-weight: bold;">PURtel</td>
                      <td align="left" valign="bottom" style="font-family: Verdana, Arial, Helvetica, sans-serif; padding:0px 0px 26px 0px; font-size: 8px; color: #000000;">&reg;</td>
                    </tr>
                    <? */ ?>

                    <tr>
                      <td align="left"><img src="images/logo1.jpg" border="0" alt="PURtel.com GmbH"></td>
                      <td align="right">&nbsp;<? if($last_wid=="503242") { 
                        //echo "[$wid][$last_wid][$slnr][$last_slnr]"
                        ?><a href="http://www.television-europe.tv" target="_blank"><img src="spaw/uploads/images/Bild 22.png" border="0" alt="International Talk"></a><? } ?></td>
                    </tr>
                  </table>
                </td>
                <td>&nbsp;</td>
                <td align="right"></td>
              </tr>
            </table>
          </td>
          <td width="4">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="4"><img src="images/menu_start.jpg" width="4" height="22"></td>
          <td background="images/menu_back.jpg">
            <table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="60"><a href="index.php<?=$suffix?$suffix."&site=start":""?>" class="menu">Startseite</a></td>
                <? if(!$sessionid) { ?>
                <td width="2"><img src="images/menu_seperator.gif" width="2" height="20"></td>
                <td><a href="https://<?=$_SERVER['HTTP_HOST']?>/index.php<?=$suffix?$suffix."&site=start&link=anmelden":"?link=anmelden"?>" class="menu">Anmelden / Tarife</a></td>
		<td width="2"><img src="images/menu_seperator.gif" width="2" height="20"></td>
		<td><a href="index.php<?=$suffix?$suffix."&site=start&link=vertriebspartner":"?link=vertriebspartner"?>" class="menu">Vertriebspartner</a></td>
		<td width="2"><img src="images/menu_seperator.gif" width="2" height="20"></td>
                <td><a href="https://<?=$_SERVER['HTTP_HOST']?>/index.php<?=$suffix?$suffix."&site=start&link=login":"?link=login"?>" class="menu">Login</a></td>
                <? } ?>
                <? /* ?>
                <td width="2"><img src="images/menu_seperator.gif" width="2" height="20"></td>
                <td><a href="index.php<?=$suffix?$suffix."&site=preise&link=preise":"?site=preise&link=preise"?>" class="menu">Preise</a></td>
                <? */ ?>
                <td width="2"><img src="images/menu_seperator.gif" width="2" height="20"></td>
                <td><a href="index.php<?=$suffix?$suffix."&site=start&link=kontakt":"?link=kontakt"?>" class="menu">Kontakt</a></td>
                <td width="2"><img src="images/menu_seperator.gif" width="2" height="20"></td>
                <td><a href="index.php<?=$suffix?$suffix."&site=start&link=impressum":"?link=impressum"?>" class="menu">Impressum</a></td>
              </tr>
            </table>
          </td>
          <td width="4"><img src="images/menu_end.jpg" width="4" height="22"></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td height="380" valign="top">
<? } /* Ende !$login_only */ ?>
    

    
      <? if(!$login_only) { ?>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
      <? } else { ?>
      <table width="992" align="center" border="0" cellpadding="0" cellspacing="0">
      <? if($custom_header) { echo $custom_header; } ?>
      <? } ?>
        <tr>
          <? if(!$login_only) { ?><td width="4" background="images/menu_start.jpg">&nbsp;</td><? } /* Ende !$login_only */ ?>
          <td id="leftmenu" width="200" valign="top" class="CustomColor"> <br>
            <br>
            
            

            <? if(!$suffix || ($preise && !$username)) { ?>
            <? if(!$login_only) { ?>
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td align="center" style="font-family: Verdana, Arial, Helvetica, sans-serif; padding:0px 0px 0px 0px; font-size: 20px; color: #FFFFFF; font-weight: normal;">
                Moderne
                </td>
              </tr>
              <tr>
                <td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 3px;">&nbsp;</td>
              </tr>
              <tr>
                <td align="center" style="font-family: Verdana, Arial, Helvetica, sans-serif; padding:0px 0px 0px 0px; font-size: 20px; color: #FFFFFF; font-weight: normal;">
                Kommunikation
                </td>
              </tr>
              <tr>
                <td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 3px;">&nbsp;</td>
              </tr>
              <tr>
                <td align="center" style="font-family: Verdana, Arial, Helvetica, sans-serif; padding:0px 0px 0px 0px; font-size: 20px; color: #FFFFFF; font-weight: normal;">
                fr jedermann
                </td>
              </tr>
              <tr>
                <td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 3px;">&nbsp;</td>
              </tr>
              <tr>
                <td align="center" style="font-family: Verdana, Arial, Helvetica, sans-serif; padding:0px 0px 0px 0px; font-size: 20px; color: #FFFFFF; font-weight: normal;">
                leicht gemacht.
                </td>
              </tr>
              <tr>
                <td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 20px;">&nbsp;</td>
              </tr>
              <tr>
                <td align="center">
                <a href="javascript:WebOeffnen('sow/index.php?wid=<? echo $last_wid; ?>&produkt=53', 'SoW')" style="font-family: Verdana, Arial, Helvetica, sans-serif; padding:0px 0px 0px 0px; font-size: 14px; color: #FFFFFF; font-weight: normal;"><b><font color="red">NEU!</font> Jetzt testen!</b><br>Stream over Web (SoW)<br><b>Jetzt mit <font color="red">VIDEO</font>!</b></A>
                </td>
              </tr>
              <tr>
                <td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 3px;">&nbsp;</td>
              </tr>
              <tr>
                <td align="center">
                <a href="javascript:WebOeffnen('sow/index.php?wid=<? echo $last_wid; ?>&produkt=53', 'SoW')" style="font-family: Verdana, Arial, Helvetica, sans-serif; padding:0px 0px 0px 0px; font-size: 14px; color: #FFFFFF; font-weight: normal;"><img src="images/sow_175" border="0" alt="SoW-Phone im Loginbereich!"></a>
                </td>
              </tr>
              <tr>
                <td align="center">
                <a href="javascript:WebOeffnen('sow/index.php?wid=<? echo $last_wid; ?>&produkt=53', 'SoW')" style="font-family: Verdana, Arial, Helvetica, sans-serif; padding:0px 0px 0px 0px; font-size: 9px; color: #FFFFFF; font-weight: normal;"><i>Telefonieren direkt auf der Webseite</i></a>
                </td>
              </tr>
              <tr>
                <td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 20px;">&nbsp;</td>
              </tr>
              <tr>
                <td align="center">
                <img src="images/initiative_mittelstand.gif" border="0" alt="PURtel.com ist Innovationspreistrger und Kategoriesieger 2007 in der Kategorie VoIP">
                </td>
              </tr>
              <tr>
                <td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 20px;">&nbsp;</td>
              </tr>
            </table>
            <? } /* Ende !$login_only */ ?>
            <? } else {


                if($site=="start" || $site=="anschluss") { $start_suffix = "act"; $site_menue_start = "$name_start"; } else { $start_suffix = "def"; $site_menue_start = "<a href=\"index.php".$suffix."&site=start&link=login\">$name_start</a>"; }
                if($site=="evn") { $evn_suffix = "act"; $site_menue_evn = "$name_evn"; } else { $evn_suffix = "def"; $site_menue_evn = "<a href=\"index.php".$suffix."&site=evn&link=login\">$name_evn</a>"; }
                /*
                if($site=="anschluss") { $site_menue_anschluss = "<b>&nbsp;&nbsp;&nbsp;&nbsp;Anschluss</b>"; } else { $site_menue_anschluss = "<a href=\"index.php".$suffix."&site=anschluss&link=login\">&#8226;&nbsp;Anschluss</a>"; }
                */
                if($site=="anrufbeantworter") { $mailbox_suffix = "act"; $site_menue_anrufbeantworter = "$name_mailbox"; } else { $mailbox_suffix = "def"; $site_menue_anrufbeantworter = "<a href=\"index.php".$suffix."&site=anrufbeantworter&link=login\">$name_mailbox</a>"; }
                if($site=="telefonzelle") { $webcall_suffix = "act"; $site_menue_telefonzelle = "$name_webcall"; } else { $webcall_suffix = "def"; $site_menue_telefonzelle = "<a href=\"index.php".$suffix."&site=telefonzelle&link=login\">$name_webcall</a>"; }
                if($site=="telefonbuch") { $kontakte_suffix = "act"; $site_menue_telefonbuch = "$name_kontakte"; } else { $kontakte_suffix = "def"; $site_menue_telefonbuch = "<a href=\"index.php".$suffix."&site=telefonbuch&link=login\">$name_kontakte</a>"; }
                if($site=="pbx") { $acd_suffix = "act"; $site_menue_pbx = "$name_acd"; } else { $acd_suffix = "def"; $site_menue_pbx = "<a href=\"index.php".$suffix."&site=pbx&link=login\">$name_acd</a>"; }
                if($site=="panel") { $pbx_suffix = "act"; $site_menue_panel = "$name_pbx"; } else { $pbx_suffix = "def"; $site_menue_panel = "<a href=\"index.php".$suffix."&site=panel&link=login&marke=Nebenstellen\">$name_pbx</a>"; }
                if($username==500000 || $username==500004 || $username==500009 || $username==500011 || $username==500012) {
                  if($site=="fax") { $fax_suffix = "act"; $site_menue_fax = "$name_fax"; } else { $fax_suffix = "def"; $site_menue_fax = "<a href=\"index.php".$suffix."&site=fax&link=login&marke=Neues Fax\">$name_fax</a>"; }
                } else {
                  if($site=="fax") { $fax_suffix = "act"; $site_menue_fax = "$name_fax"; } else { $fax_suffix = "def"; $site_menue_fax = "<a href=\"index.php".$suffix."&site=fax&link=login&marke=Neues Fax\">$name_fax</a>"; }
                }
                if($site=="einstellungen") { $einstellungen_suffix = "act"; $site_menue_einstellungen = "$name_verwaltung"; } else { $einstellungen_suffix = "def"; $site_menue_einstellungen = "<a href=\"index.php".$suffix."&site=einstellungen&link=login\">$name_verwaltung</a>"; }


            ?>
            <table class="leftmenu" width="95%" border="0" align="center" cellpadding="2" cellspacing="0">
              <? if($show_start!="2") { ?><tr><td id="start" class="<? echo $start_suffix; ?>"><? echo $site_menue_start; ?></td></tr><? } ?>
              <? if($show_evn!="2") { ?><tr><td id="evn" class="<? echo $evn_suffix; ?>"><? echo $site_menue_evn; ?></td></tr><? } ?>
              <? /* ?><tr><td><? echo $site_menue_anschluss; ?></td></tr><? */ ?>

              <? if($show_mailbox!="2") { ?><tr><td id="mailbox" class="<? echo $mailbox_suffix; ?>"><? echo $site_menue_anrufbeantworter; ?></td></tr><? } ?>
              <? if($admin_is_webcall && $is_webcall && ($admin_kundennummer=="100001" || $admin_kundennummer=="100009" || $admin_kundennummer=="100013" || $admin_kundennummer=="100022")) { ?>
              <? if($show_webcall!="2") { ?><tr><td id="webcall" class="<? echo $webcall_suffix; ?>"><? echo $site_menue_telefonzelle; ?></td></tr><? } ?>
              <? } ?>
              <? if(($admin_is_webcall && $is_webcall) || ($admin_is_pbx && $is_pbx)) { ?>
              <? if($show_kontakte!="2") { ?><tr><td id="kontakte" class="<? echo $kontakte_suffix; ?>"><? echo $site_menue_telefonbuch; ?></td></tr><? } ?>
              <? } ?>
              <? if($admin_is_pbx && $is_pbx && ($admin_kundennummer=="100001" || $admin_kundennummer=="100009" || $admin_kundennummer=="100013" || $admin_kundennummer=="100022")) { ?>
              <? if($is_pbx_advanced && ($username==500000 || $username==500004 || $username==500009 || $username==500011 || $username==500012)) { ?>
              <? if($show_pbx!="2") { ?><tr><td id="pbx" class="<? echo $pbx_suffix; ?>"><? echo $site_menue_panel; ?></td></tr><? } ?>
              <? } ?>
              <? if($show_acd!="2") { ?><tr><td id="acd" class="<? echo $acd_suffix; ?>"><? echo $site_menue_pbx; ?></td></tr><? } ?>
              <? if($is_pbx_advanced && ($username==500000 || $username==500004 || $username==500009 || $username==500011 || $username==500012)) { ?><? } ?>
              <? } ?>
              <? if($show_fax!="2") { ?><tr><td id="fax" class="<? echo $fax_suffix; ?>"><? echo $site_menue_fax; ?></td></tr><? } ?>
              <? if($show_verwaltung!="2") { ?><tr><td id="einstellungen" class="<? echo $einstellungen_suffix; ?>"><? echo $site_menue_einstellungen; ?></td></tr><? } ?>
              <tr><td>&nbsp;</td></tr>
              <? if($show_logout!="2") { ?><tr><td id="logout" class="def"><a href="index.php<? echo $suffix ?>&site=logout&link=login"><? echo $name_logout; ?></a></td></tr><? } ?>

              <? if($username=="500000" || $admin_kundennummer=="100022") { ?><tr><td>
              <br>
              <form action="index.php" method="post" style="display: inline;">
              <input type="hidden" name="senden" value="1">
              <input type="hidden" name="username" value="<? echo $username; ?>">
              <input type="hidden" name="sessionid" value="<? echo $sessionid; ?>">
              <input type="hidden" name="marke" value="<? echo $marke; ?>">
              <input name="site" type="hidden" value="<? echo $site; ?>">
              <input name="link" type="hidden" value="login">
              <input name="vollbild" type="hidden" value="<? echo $vollbild; ?>">
              <? 
              if($language=="de") { $de_selected = " selected"; } else { $de_selected = ""; }
              if($language=="en") { $en_selected = " selected"; } else { $en_selected = ""; }
              ?>
              <input name="aktion" type="hidden" value="changelanguage">
              <select name="language" onChange="javascript:Verlinken2(this.form);">
              <option value="de"<? echo $de_selected; ?>>Deutsch</option>
              <option value="en"<? echo $en_selected; ?>>English</option>
              </select>
              </form>
              </td></tr><? } ?>

              <? if($admin_kundennummer=="108196") { ?>
              <tr><td>&nbsp;</td></tr>
              <? if($show_dokumente!="2") { ?><tr><td id="doku"><a href="doku20060601.pdf" target="_blank"><? echo $name_dokumente; ?></a></td></tr><? } ?>
              <? } ?>
            </table>
            <?}?>
          </td>
          <td id="content" height="590" valign="top">


            <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td valign="top"><? } ?><? if($link){ include("templates/$link.tpl.php"); } else { include("templates/start.tpl.php"); }?><? if(!$vollbild) { ?></td>
              </tr>
            </table>
            <div id="content_help">
            <? echo $content_hilfe; ?>
            <? echo $content_linkextern; ?>
            <? echo $content_bearbeiten; ?>
            <? echo $content_operator; ?>
            <? echo $content_admin_system; ?>
            </div>
          </td>
          <? if(!$login_only) { ?><td width="4" background="images/menu_end.jpg">&nbsp;</td><? } /* Ende !$login_only */ ?>
        </tr>
      </table>
      
      
      
      
<? if(!$login_only) { ?>
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="4"><img src="images/menu_start.jpg" width="4" height="22"></td>
          <td background="images/menu_back.jpg">&nbsp;</td>
          <td width="4"><img src="images/menu_end.jpg" width="4" height="22"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<? } // Ende !$login_only ?>

<? } ?>
<script language="JavaScript" type="text/javascript" src="wz_tooltip.js"></script>
<? if($marke=="Nebenstellen") { ?><script language="JavaScript" type="text/javascript">start();</script><? } ?>
</body>
</html>
