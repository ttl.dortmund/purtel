<?php



### Produkteditor

$result = db_query("SELECT * FROM PURtel_nebenstellen_produkte WHERE kundennummer = '$admin_kundennummer' AND  id = '$produkt' LIMIT 1;");
$row = db_fetch_row($result);
$produkt_id = $row[0];
$produkt_kundennummer = $row[1];
$produkt_bezeichnung = $row[2];
$produkt_beschreibung_lang = $row[3];
$produkt_kommentar = $row[4];
$produkt_aktiv = $row[5];
$produkt_aktiv_admin = $row[6];
$produkt_gueltig_ab = $row[7];
$produkt_gueltig_bis = $row[8];
$produkt_max_channels = $row[9];
$produkt_max_channels_vertrag = $row[10];
$produkt_max_numbers = $row[11];
$produkt_max_numbers_vertrag = $row[12];
$produkt_max_fax = $row[13];
$produkt_max_fax_vertrag = $row[14];
$produkt_max_pool = $row[15];
$produkt_max_pool_vertrag = $row[16];
$produkt_max_accounts_vertrag = $row[17];
$produkt_is_voip = $row[18];
$produkt_is_webcall = $row[19];
$produkt_is_pbx = $row[20];
$produkt_flat = $row[21];
$produkt_faxflat = $row[22];
$produkt_handelsspanne = $row[23];
$produkt_taktung = $row[24];
$produkt_postpaid = $row[25];
$produkt_kredit = $row[26];
$produkt_handelsspanne_national = $row[27];
$produkt_handelsspanne_international = $row[28];
$produkt_handelsspanne_fax = $row[29];
$produkt_bereitstellung = $row[30];
$produkt_basisentgelt = $row[31];
$produkt_laufzeit = $row[32];
$produkt_paket_telefon_gassen = $row[33];
$produkt_paket_telefon_menge = $row[34];
$produkt_paket_telefon_art = $row[35];
$produkt_paket_telefon_haltbar = $row[36];
$produkt_paket_fax_gassen = $row[37];
$produkt_paket_fax_menge = $row[38];
$produkt_paket_fax_art = $row[39];
$produkt_paket_fax_haltbar = $row[40];
$produkt_paket_fax_menge_telefonie = $row[41];

$produkt_tarif_wechseln_alle = $row[42];
$produkt_tarif_wechseln = $row[43];
$produkt_prepaid_tarif_wechseln = $row[44];
$produkt_prepaid_tarif_zuruecksetzen = $row[45];
$produkt_ablauf_wechseln = $row[46];
$produkt_lebensdauer = $row[47];
$produkt_lebensdauer_wechseln = $row[48];

$produkt_switch_kredit = $row[65];
$produkt_switch_zahlart = $row[66];
$produkt_switch_gesperrt = $row[67];
$produkt_neuanlage_gesperrt = $row[68];
$produkt_limit_0900x = $row[69];
$produkt_limit_118x = $row[70];
$produkt_limit_018x = $row[71];

    $result = db_query("SELECT limit_0900x, limit_118x, limit_018x FROM PURtel_nebenstellen_admin WHERE kundennummer = '$admin_kundennummer' LIMIT 1;");
    $row = db_fetch_row($result);
    $produkt_max_limit_0900x = $row[0];
    $produkt_max_limit_118x = $row[1];
    $produkt_max_limit_018x = $row[2];

//if($admin_kundennummer=="100022") { mail("vg@purtel.com","MWD 1 [".$produkt_limit_0900x."/".$produkt_max_limit_0900x."] [".$produkt_limit_118x."/".$produkt_max_limit_118x."] [".$produkt_limit_018x."/".$produkt_max_limit_018x."]","","from:produkteditor@purtel.com"); }

    if($produkt_limit_0900x>$produkt_max_limit_0900x) { $produkt_limit_0900x = $produkt_max_limit_0900x; }
    if($produkt_limit_118x>$produkt_max_limit_118x) { $produkt_limit_118x = $produkt_max_limit_118x; }
    if($produkt_limit_018x>$produkt_max_limit_018x) { $produkt_limit_018x = $produkt_max_limit_018x; }


if(!$produkt_id) {
  echo "-ERR 047 Produkt [$produkt] [$admin_kundennummer] existiert nicht";
  exit;
}


//mail("vg@purtel.com","create: [$kundennummer_extern] [$telefon]","","from:produkteditor@purtel.com");

$whitelabel_answer = create_account();

if(substr($whitelabel_answer,0,1)=="+") {
  
  ### Array erzeugen
  $whitelabel_answer_array = explode(";",$whitelabel_answer);

  ### Benutzername ermitteln
  $benutzername_array = explode("=",$whitelabel_answer_array[1]);
  $benutzername = $benutzername_array[1];

  ### Passwort ermitteln
  $passwort_array = explode("=",$whitelabel_answer_array[2]);
  $passwort = $passwort_array[1];
  
  if(!$kundennummer_extern) {
    $kundennummer_extern = $benutzername;
    $result = db_query("UPDATE PURtel_nebenstellen SET
      kundennummer_extern = '".$kundennummer_extern."'
    WHERE admin_kundennummer = '$admin_kundennummer' AND rufnummer = '$benutzername';");
    
    if($produkt_prepaid_tarif_zuruecksetzen && !$produkt_postpaid && ($produkt_id!=$produkt_prepaid_tarif_zuruecksetzen)) {
  
      $produkt_zuruecksetzen = $produkt_bezeichnung;

      $result = db_query("SELECT * FROM PURtel_nebenstellen_produkte WHERE kundennummer = '$admin_kundennummer' AND  id = '$produkt_prepaid_tarif_zuruecksetzen' LIMIT 1;");
      $row = db_fetch_row($result);
      $produkt_id = $row[0];
      $produkt_kundennummer = $row[1];
      $produkt_bezeichnung = $row[2];
      $produkt_beschreibung_lang = $row[3];
      $produkt_kommentar = $row[4];
      $produkt_aktiv = $row[5];
      $produkt_aktiv_admin = $row[6];
      $produkt_gueltig_ab = $row[7];
      $produkt_gueltig_bis = $row[8];
      $produkt_max_channels = $row[9];
      $produkt_max_channels_vertrag = $row[10];
      $produkt_max_numbers = $row[11];
      $produkt_max_numbers_vertrag = $row[12];
      $produkt_max_fax = $row[13];
      $produkt_max_fax_vertrag = $row[14];
      $produkt_max_pool = $row[15];
      $produkt_max_pool_vertrag = $row[16];
      $produkt_max_accounts_vertrag = $row[17];
      $produkt_is_voip = $row[18];
      $produkt_is_webcall = $row[19];
      $produkt_is_pbx = $row[20];
      $produkt_flat = $row[21];
      $produkt_faxflat = $row[22];
      $produkt_handelsspanne = $row[23];
      $produkt_taktung = $row[24];
      $produkt_postpaid = $row[25];
      $produkt_kredit = $row[26];
      $produkt_handelsspanne_national = $row[27];
      $produkt_handelsspanne_international = $row[28];
      $produkt_handelsspanne_fax = $row[29];
      $produkt_bereitstellung = $row[30];
      $produkt_basisentgelt = $row[31];
      $produkt_laufzeit = $row[32];
      $produkt_paket_telefon_gassen = $row[33];
      $produkt_paket_telefon_menge = $row[34];
      $produkt_paket_telefon_art = $row[35];
      $produkt_paket_telefon_haltbar = $row[36];
      $produkt_paket_fax_gassen = $row[37];
      $produkt_paket_fax_menge = $row[38];
      $produkt_paket_fax_art = $row[39];
      $produkt_paket_fax_haltbar = $row[40];
      $produkt_paket_fax_menge_telefonie = $row[41];

      $produkt_tarif_wechseln_alle = $row[42];
      $produkt_tarif_wechseln = $row[43];
      $produkt_prepaid_tarif_wechseln = $row[44];
      $produkt_prepaid_tarif_zuruecksetzen = $row[45];
      $produkt_ablauf_wechseln = $row[46];
      $produkt_lebensdauer = $row[47];
      $produkt_lebensdauer_wechseln = $row[48];

      $produkt_switch_kredit = $row[65];
      $produkt_switch_zahlart = $row[66];
      $produkt_switch_gesperrt = $row[67];
      $produkt_neuanlage_gesperrt = $row[68];
      $produkt_limit_0900x = $row[69];
      $produkt_limit_118x = $row[70];
      $produkt_limit_018x = $row[71];

    $result = db_query("SELECT limit_0900x, limit_118x, limit_018x FROM PURtel_nebenstellen_admin WHERE kundennummer = '$admin_kundennummer' LIMIT 1;");
    $row = db_fetch_row($result);
    $produkt_max_limit_0900x = $row[0];
    $produkt_max_limit_118x = $row[1];
    $produkt_max_limit_018x = $row[2];

//if($admin_kundennummer=="100022") { mail("vg@purtel.com","MWD 2 [".$produkt_limit_0900x."/".$produkt_max_limit_0900x."] [".$produkt_limit_118x."/".$produkt_max_limit_118x."] [".$produkt_limit_018x."/".$produkt_max_limit_018x."]","","from:produkteditor@purtel.com"); }

    if($produkt_limit_0900x>$produkt_max_limit_0900x) { $produkt_limit_0900x = $produkt_max_limit_0900x; }
    if($produkt_limit_118x>$produkt_max_limit_118x) { $produkt_limit_118x = $produkt_max_limit_118x; }
    if($produkt_limit_018x>$produkt_max_limit_018x) { $produkt_limit_018x = $produkt_max_limit_018x; }


    }
  }

    $result = db_query("SELECT limit_0900x, limit_118x, limit_018x FROM PURtel_nebenstellen_admin WHERE kundennummer = '$admin_kundennummer' LIMIT 1;");
    $row = db_fetch_row($result);
    $produkt_max_limit_0900x = $row[0];
    $produkt_max_limit_118x = $row[1];
    $produkt_max_limit_018x = $row[2];

//if($admin_kundennummer=="100022") { mail("vg@purtel.com","MWD 3 [".$produkt_limit_0900x."/".$produkt_max_limit_0900x."] [".$produkt_limit_118x."/".$produkt_max_limit_118x."] [".$produkt_limit_018x."/".$produkt_max_limit_018x."]","","from:produkteditor@purtel.com"); }

    if($produkt_limit_0900x>$produkt_max_limit_0900x) { $produkt_limit_0900x = $produkt_max_limit_0900x; }
    if($produkt_limit_118x>$produkt_max_limit_118x) { $produkt_limit_118x = $produkt_max_limit_118x; }
    if($produkt_limit_018x>$produkt_max_limit_018x) { $produkt_limit_018x = $produkt_max_limit_018x; }


  $result = db_query("UPDATE PURtel_nebenstellen SET
  max_channels = '".$produkt_max_channels."',
  max_channels_vertrag = '".$produkt_max_channels_vertrag."',
  max_numbers = '".$produkt_max_numbers."',
  max_numbers_vertrag = '".$produkt_max_numbers_vertrag."',
  max_pool = '".$produkt_max_pool."',
  max_pool_vertrag = '".$produkt_max_pool_vertrag."',
  max_fax = '".$produkt_max_fax."',
  max_fax_vertrag = '".$produkt_max_fax_vertrag."',
  max_accounts_vertrag = '".$produkt_max_accounts_vertrag."',
  is_voip = '".$produkt_is_voip."',
  is_pbx = '".$produkt_is_pbx."',
  is_webcall = '".$produkt_is_webcall."',
  handelsspanne = '".$produkt_handelsspanne."',
  taktung = '".$produkt_taktung."',
  nolimit = '".$produkt_postpaid."',
  kredit = '".$produkt_kredit."',
  limit_0900x = '".$produkt_limit_0900x."',
  limit_118x = '".$produkt_limit_118x."',
  limit_018x = '".$produkt_limit_018x."'

  WHERE admin_kundennummer = '$admin_kundennummer' AND kundennummer_extern = '$kundennummer_extern';");

//if($admin_kundennummer=="100022") { mail("vg@purtel.com","MWD 4 [".mysql_error()."] [".$produkt_limit_0900x."/".$produkt_max_limit_0900x."] [".$produkt_limit_118x."/".$produkt_max_limit_118x."] [".$produkt_limit_018x."/".$produkt_max_limit_018x."]","","from:produkteditor@purtel.com"); }

      if($produkt_switch_kredit=="1") {
        $result = db_query("UPDATE PURtel_nebenstellen SET  
          kredit = '".$produkt_kredit."'
        WHERE admin_kundennummer = '$admin_kundennummer' AND kundennummer_extern = '$kundennummer_extern';");
      }

      if($produkt_switch_zahlart=="1") {
        $result = db_query("UPDATE PURtel_nebenstellen SET  
          nolimit = '".$produkt_postpaid."'
        WHERE admin_kundennummer = '$admin_kundennummer' AND kundennummer_extern = '$kundennummer_extern';");
      }

      if($produkt_switch_gesperrt=="1") {
        $result = db_query("SELECT gesperrt FROM PURtel_nebenstellen WHERE admin_kundennummer = '$admin_kundennummer' AND kundennummer_extern = '$kundennummer_extern' LIMIT 1;");
        $row = db_fetch_row($result);
        $check_status_to_switch = $row[0];

        if($check_status_to_switch=="1") { $produkt_gesperrt = "0"; } else { $produkt_gesperrt = "1"; }
        if($produkt_neuanlage_gesperrt=="1") { $produkt_gesperrt = "1"; }
    
        $result = db_query("UPDATE PURtel_nebenstellen SET  
          gesperrt = '".$produkt_gesperrt."'
        WHERE admin_kundennummer = '$admin_kundennummer' AND kundennummer_extern = '$kundennummer_extern';");
      }


  ### Array erzeugen
  $whitelabel_answer_array = explode(";",$whitelabel_answer);

  ### Benutzername ermitteln
  $benutzername_array = explode("=",$whitelabel_answer_array[1]);
  $benutzername = $benutzername_array[1];

  ### Passwort ermitteln
  $passwort_array = explode("=",$whitelabel_answer_array[2]);
  $passwort = $passwort_array[1];

  if($produkt_gesperrt=="1" || $konto_von!="") { $produkt_gesperrt_aktiv = "0"; } else { $produkt_gesperrt_aktiv = "1"; }
  
  $result = db_query("INSERT INTO PURtel_nebenstellen_crossselling (
   id,
   kundennummer,
   rufnummer,
   datum,
   produkt_id,
   einrichtung,
   grundgebuehr,
   laufzeit,
   aktiv,
   produktcreatorid,
   kundennummer_extern
  ) VALUES (
   NULL,
   '$admin_kundennummer',
   '$benutzername',
   '".date("Ymd",time())."',
   '',
   '$produkt_bereitstellung',
   '$produkt_basisentgelt',
   '$produkt_laufzeit',
   '$produkt_gesperrt_aktiv',
   '$produkt_id',
   '$kundennummer_extern'
  )");
  
  if($produkt_paket_telefon_menge) {
    $result = db_query("INSERT INTO PURtel_nebenstellen_pakete (
     id,
     rufnummer,
     kundennummer,
     kundennummer_extern,
     gasse,
     haltbar,
     menge,
     art,
     datum
    ) VALUES (
     NULL,
     '$benutzername',
     '$admin_kundennummer',
     '$kundennummer_extern',
     '$produkt_paket_telefon_gassen',
     '$produkt_paket_telefon_haltbar',
     '$produkt_paket_telefon_menge',
     '$produkt_paket_telefon_art',
     '".date("Ymd",time())."'
    )");
  }
  
  if($produkt_paket_fax_menge) {
    $result = db_query("INSERT INTO PURtel_nebenstellen_paketefax (
     id,
     rufnummer,
     kundennummer,
     kundennummer_extern,
     gasse,
     haltbar,
     menge,
     art,
     datum
    ) VALUES (
     NULL,
     '$benutzername',
     '$admin_kundennummer',
     '$kundennummer_extern',
     '$produkt_paket_fax_gassen',
     '$produkt_paket_fax_haltbar',
     '$produkt_paket_fax_menge',
     '$produkt_paket_fax_art',
     '".date("Ymd",time())."'
    )");
  }
  /*
  if($produkt_zuruecksetzen) {
    $produkt_zuruecksetzen = ";produkt_zuruecksetzen=".$produkt_zuruecksetzen."";
  }
  */
  require("_db_config3.inc.php");
  
  $result = db_query("SELECT durchwahl FROM pool_".$pool_kopfnummer."_4  WHERE kundennummer = '$admin_kundennummer' AND rufnummer = '$benutzername' AND serverhome = '$serverhome' LIMIT 1;");
  $row = db_fetch_row($result);
  if($row[0]) {
    $neue_poolnummer = $pool_kopfnummer.$row[0];
  } else {
    $neue_poolnummer = "";
  }

  require("_db_config.inc.php");

  ### Ausgabe erzeugen
  echo "+OK;produkt=".$produkt.";benutzername=".$benutzername.";passwort=".$passwort.";produkt_bezeichnung=".$produkt_bezeichnung.";produkt_zuruecksetzen=".$produkt_zuruecksetzen.";poolnummer=".$neue_poolnummer.";"; //.mysql_error()

  
  //mailversand
  $result = $dbPurtel->query("SELECT fromemail, betreff, mailtext, senden, fromname, mailhtml, characterset, mailhtml2, mailankunde, mailanmich, meineemail, mailtext2 FROM PURtel_nebenstellen_anmeldemailadmin WHERE kundennummer = '$admin_kundennummer' LIMIT 1;");
  $row = $result->fetch(PDO::FETCH_NUM);
  $amail = $row[0];
  $betreff = $row[1];
  $mailtext = $row[2];
  $senden = $row[3];
  $aname = $row[4];
  $mailhtml = stripslashes($row[5]);
  $charset = $row[6];
  $mailhtml2 = stripslashes($row[7]);
  $mailankunde = $row[8];
  $mailanmich = $row[9];
  $meineemail = $row[10];
  $mailtext2 = $row[11];
  $att1 = "";
  $att2 = "";
  $att3 = "";
  $att4 = "";
  $att51 = "";

  if($anrede=="1") {
      $anrede = "Herr";
      $anrede_im_text = "Sehr geehrter Herr $nachname";
  }
  if($anrede=="2") {
      $anrede = "Frau";
      $anrede_im_text = "Sehr geehrte Frau $nachname";
  }
  $anredetext = $anrede_im_text; 
  
  $create_rufnummer = $benutzername;
  $create_telpasswort = $passwort;  

  $zweimails = 0;
  if (strlen($mailtext2) > 0){         
      $zweimails = 1;
  }

  $mailtext = str_replace("%anrede%",$anrede,$mailtext);
  $mailtext = str_replace("%anredetext%",$anredetext,$mailtext);
  $mailtext = str_replace("%produkt_zuruecksetzen%",$produkt_zuruecksetzen,$mailtext);
  $mailtext = str_replace("%produkt_zuruecksetzen_text%",$produkt_zuruecksetzen_text,$mailtext);
  $mailtext = str_replace("%nachricht%",$nachricht,$mailtext);
  $mailtext = str_replace("%anrede_im_text%",$anrede_im_text,$mailtext);
  $mailtext = str_replace("%gebuchte_module%",$gebuchte_module,$mailtext);
  $mailtext = str_replace("%create_rufnummer%",$create_rufnummer,$mailtext);

  if ($zweimails){       
    $mailtext = str_replace("%create_telpasswort%","",$mailtext);// $passwort l�schen         
    $mailtext = str_replace("Anschluss Passwort:","",$mailtext);//       
    $mailtext = str_replace("Password:","",$mailtext);//       
  }else{
    $mailtext = str_replace("%create_telpasswort%",$create_telpasswort,$mailtext); 
  }

  $mailtext = str_replace("%create_vmpasswort%",$create_vmpasswort,$mailtext);
  $mailtext = str_replace("%firma%",$firma,$mailtext);
  $mailtext = str_replace("%anredetext%",$anredetext,$mailtext);
  $mailtext = str_replace("%vorname%",$vorname,$mailtext);
  $mailtext = str_replace("%nachname%",$nachname,$mailtext);
  $mailtext = str_replace("%adresse%",$adresse,$mailtext);
  $mailtext = str_replace("%haus_nr%",$haus_nr,$mailtext);
  $mailtext = str_replace("%zusatz%",$zusatz,$mailtext);
  $mailtext = str_replace("%plz%",$plz,$mailtext);
  $mailtext = str_replace("%ort%",$ort,$mailtext);
  $mailtext = str_replace("%ot%",$ot,$mailtext);
  $mailtext = str_replace("%land%",$land,$mailtext);
  $mailtext = str_replace("%zeitzone%",$zeitzone,$mailtext);
  $mailtext = str_replace("%waehrung%",$waehrung,$mailtext);
  $mailtext = str_replace("%telefon%",$telefon,$mailtext);
  $mailtext = str_replace("%mobile%",$mobile,$mailtext);
  $mailtext = str_replace("%email%",$email,$mailtext);
  $mailtext = str_replace("%steuernummer%",$steuernummer,$mailtext);
  $mailtext = str_replace("%inhaber%",$inhaber,$mailtext);
  $mailtext = str_replace("%bank%",$bank,$mailtext);
  $mailtext = str_replace("%blz%",$blz,$mailtext);
  $mailtext = str_replace("%kontonr%",$kontonr,$mailtext);
  $mailtext = str_replace("%gtag%",$gtag,$mailtext);
  $mailtext = str_replace("%gmonat%",$gmonat,$mailtext);
  $mailtext = str_replace("%gjahr%",$gjahr,$mailtext);  

  $mailtext2 = str_replace("%anrede%",$anrede,$mailtext2);
  $mailtext2 = str_replace("%anredetext%",$anredetext,$mailtext2);
  $mailtext2 = str_replace("%produkt_zuruecksetzen%",$produkt_zuruecksetzen,$mailtext2);
  $mailtext2 = str_replace("%produkt_zuruecksetzen_text%",$produkt_zuruecksetzen_text,$mailtext2);
  $mailtext2 = str_replace("%nachricht%",$nachricht,$mailtext2);
  $mailtext2 = str_replace("%anrede_im_text%",$anrede_im_text,$mailtext2);
  $mailtext2 = str_replace("%gebuchte_module%",$gebuchte_module,$mailtext2);

  if ($zweimails){          
        $mailtext2 = str_replace("Anschluss Nummer:","",$mailtext2);
        $mailtext2 = str_replace("Username:","",$mailtext2);
        $mailtext2 = str_replace("%create_rufnummer%","",$mailtext2);
  }
  $mailtext2 = str_replace("%create_telpasswort%",$create_telpasswort,$mailtext2);

  $mailtext2 = str_replace("%create_vmpasswort%",$create_vmpasswort,$mailtext2);
  $mailtext2 = str_replace("%firma%",$firma,$mailtext2);
  $mailtext2 = str_replace("%anredetext%",$anredetext,$mailtext2);
  $mailtext2 = str_replace("%vorname%",$vorname,$mailtext2);
  $mailtext2 = str_replace("%nachname%",$nachname,$mailtext2);
  $mailtext2 = str_replace("%adresse%",$adresse,$mailtext2);
  $mailtext2 = str_replace("%haus_nr%",$haus_nr,$mailtext2);
  $mailtext2 = str_replace("%zusatz%",$zusatz,$mailtext2);
  $mailtext2 = str_replace("%plz%",$plz,$mailtext2);
  $mailtext2 = str_replace("%ort%",$ort,$mailtext2);
  $mailtext2 = str_replace("%ot%",$ot,$mailtext2);
  $mailtext2 = str_replace("%land%",$land,$mailtext2);
  $mailtext2 = str_replace("%zeitzone%",$zeitzone,$mailtext2);
  $mailtext2 = str_replace("%waehrung%",$waehrung,$mailtext2);
  $mailtext2 = str_replace("%telefon%",$telefon,$mailtext2);
  $mailtext2 = str_replace("%mobile%",$mobile,$mailtext2);
  $mailtext2 = str_replace("%email%",$email,$mailtext2);
  $mailtext2 = str_replace("%steuernummer%",$steuernummer,$mailtext2);
  $mailtext2 = str_replace("%inhaber%",$inhaber,$mailtext2);
  $mailtext2 = str_replace("%bank%",$bank,$mailtext2);
  $mailtext2 = str_replace("%blz%",$blz,$mailtext2);
  $mailtext2 = str_replace("%kontonr%",$kontonr,$mailtext2);
  $mailtext2 = str_replace("%gtag%",$gtag,$mailtext2);
  $mailtext2 = str_replace("%gmonat%",$gmonat,$mailtext2);
  $mailtext2 = str_replace("%gjahr%",$gjahr,$mailtext2);        


  $mailhtml = str_replace("%anrede%",$anrede,$mailhtml);
  $mailhtml = str_replace("%anredetext%",$anredetext,$mailhtml);
  $mailhtml = str_replace("%produkt_zuruecksetzen%",$produkt_zuruecksetzen,$mailhtml);
  $mailhtml = str_replace("%produkt_zuruecksetzen_text%",$produkt_zuruecksetzen_text,$mailhtml);
  $mailhtml = str_replace("%nachricht%",$nachricht,$mailhtml);
  $mailhtml = str_replace("%anrede_im_text%",$anrede_im_text,$mailhtml);
  $mailhtml = str_replace("%gebuchte_module%",$gebuchte_module,$mailhtml);
  $mailhtml = str_replace("%create_rufnummer%",$create_rufnummer,$mailhtml);
  $mailhtml = str_replace("%create_telpasswort%",$create_telpasswort,$mailhtml);
  $mailhtml = str_replace("%create_vmpasswort%",$create_vmpasswort,$mailhtml);
  $mailhtml = str_replace("%firma%",$firma,$mailhtml);
  $mailhtml = str_replace("%anredetext%",$anredetext,$mailhtml);
  $mailhtml = str_replace("%vorname%",$vorname,$mailhtml);
  $mailhtml = str_replace("%nachname%",$nachname,$mailhtml);
  $mailhtml = str_replace("%adresse%",$adresse,$mailhtml);
  $mailhtml = str_replace("%haus_nr%",$haus_nr,$mailhtml);
  $mailhtml = str_replace("%zusatz%",$zusatz,$mailhtml);
  $mailhtml = str_replace("%plz%",$plz,$mailhtml);
  $mailhtml = str_replace("%ort%",$ort,$mailhtml);
  $mailhtml = str_replace("%ot%",$ot,$mailhtml);
  $mailhtml = str_replace("%land%",$land,$mailhtml);
  $mailhtml = str_replace("%zeitzone%",$zeitzone,$mailhtml);
  $mailhtml = str_replace("%waehrung%",$waehrung,$mailhtml);
  $mailhtml = str_replace("%telefon%",$telefon,$mailhtml);
  $mailhtml = str_replace("%mobile%",$mobile,$mailhtml);
  $mailhtml = str_replace("%email%",$email,$mailhtml);
  $mailhtml = str_replace("%steuernummer%",$steuernummer,$mailhtml);
  $mailhtml = str_replace("%inhaber%",$inhaber,$mailhtml);
  $mailhtml = str_replace("%bank%",$bank,$mailhtml);
  $mailhtml = str_replace("%blz%",$blz,$mailhtml);
  $mailhtml = str_replace("%kontonr%",$kontonr,$mailhtml);
  $mailhtml = str_replace("%gtag%",$gtag,$mailhtml);
  $mailhtml = str_replace("%gmonat%",$gmonat,$mailhtml);
  $mailhtml = str_replace("%gjahr%",$gjahr,$mailhtml);  


  $result = $dbPurtel->query("SELECT reseller_verzeichnis FROM PURtel_nebenstellen_admin WHERE kundennummer = '".$admin_kundennummer."'");
  $row = $result->fetch(PDO::FETCH_NUM);
  $resellerverzeichnis = $row[0];
  require($resellerverzeichnis . "config.php");

  require_once('/var/www/web1/api/send_my_mail.inc.php');


  
  ### Mail an Kontrolle
  send_my_mail(100001, $meine_email, $meine_email, "vg@purtel.com", "vg@purtel.com", $betreff_prefix . " Neue Anmeldung [".$admin_kundennummer."] [".$check_admin_kundennummer."] [".$admin_check_kundennummer."]", $mailtext, "", "", "", "", "", "", "", "");
  if ($zweimails)
      send_my_mail(100001, $meine_email, $meine_email, "vg@purtel.com", "vg@purtel.com", $betreff_prefix . " Neue Anmeldung [".$admin_kundennummer."] [".$check_admin_kundennummer."] [".$admin_check_kundennummer."]", $mailtext2, "", "", "", "", "", "", "", "");

//  send_my_mail(100001, $meine_email, $meine_email, "tl@purtel.com", "tl@purtel.com", $betreff_prefix . " Neue Anmeldung [".$admin_kundennummer."] [".$check_admin_kundennummer."] [".$admin_check_kundennummer."]", $mailtext, "", "", "", "", "", "", "", "");
//  if ($zweimails)
//      send_my_mail(100001, $meine_email, $meine_email, "tl@purtel.com", "tl@purtel.com", $betreff_prefix . " Neue Anmeldung [".$admin_kundennummer."] [".$check_admin_kundennummer."] [".$admin_check_kundennummer."]", $mailtext2, "", "", "", "", "", "", "", "");

  ### Mail an Mandant
  //if($admin_kundennummer!="100096" && $admin_kundennummer!="100132" && $admin_kundennummer!="100223" && $admin_kundennummer!="100251" && $admin_kundennummer!="100254" && $admin_kundennummer!="100265" && $admin_kundennummer!="100305") {
  if($meineemail!="") { $admin_mail = $meineemail; }
  if($mailanmich=="1") {
      send_my_mail($admin_kundennummer, $amail, $aname, $admin_mail, $admin_mail, "Neue Anmeldung", $mailtext, "", "", "", "", "", "", "", "");
      if ($zweimails)
          send_my_mail($admin_kundennummer, $amail, $aname, $admin_mail, $admin_mail, "Neue Anmeldung", $mailtext2, "", "", "", "", "", "", "", "");
  }

  //if($admin_kundennummer=="100143") { $email = "loft-dsl@wemacom.de"; }
  //if($meineemail!="") { $email = $meineemail; }

  ### Mail an den Kunden
  //if($admin_kundennummer!="100057" && $admin_kundennummer!="100096" && $admin_kundennummer!="100129" && $admin_kundennummer!="100135" && $admin_kundennummer!="100174" && $admin_kundennummer!="100176" && $admin_kundennummer!="100179" && $admin_kundennummer!="100202" && $admin_kundennummer!="100207" && $admin_kundennummer!="100223" && $admin_kundennummer!="100224" && $admin_kundennummer!="100242" && $admin_kundennummer!="100251" && $admin_kundennummer!="100254" && $admin_kundennummer!="100265" && $admin_kundennummer!="100273" && $admin_kundennummer!="100296" && $admin_kundennummer!="100304" && $admin_kundennummer!="100305" && $admin_kundennummer!="100311") {            
  if($mailankunde=="1") {
    if($betreff_prefix=="") {
      if($betrefftext=="") { 
        $betrefftext = $betreff_prefix." Ihre Anmeldung"; 
      }
    } else {
      if($betreff=="") { 
        $betrefftext = $betreff_prefix; 
      } else {
        $betrefftext = $betreff_prefix. " " . $betreff;
      }
    }

    send_my_mail($admin_kundennummer, $amail, $aname, $email, $email, $betrefftext, $mailtext, "", "", "", "", "", "", "", "");    
    if ($zweimails)
        send_my_mail($admin_kundennummer, $amail, $aname, $email, $email, $betrefftext, $mailtext2, "", "", "", "", "", "", "", "");                   
  }
  

        
} else {
  echo $whitelabel_answer;
  exit;
}



?>
